package de.mo.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import de.mo.execution.ExecutionManager;

public class ClientHandler extends Thread{
	
	private Socket mClient;
	private Interface_ServerFeedback mFeedback;
	
	public ClientHandler(Socket client, Interface_ServerFeedback feedback){
		this.mClient = client;
		this.mFeedback = feedback;
	}
	
	@Override
	public void run(){
		char[] buffer = new char[200];
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mClient.getInputStream(), "UTF-8"));
			int anzahlZeichen = bufferedReader.read(buffer, 0, 200);
		    OutputStream out = mClient.getOutputStream(); 
		    DataOutputStream dos = new DataOutputStream(out);
		    MessageModel message = new MessageModel(new String(buffer, 0, anzahlZeichen));
			byte[] response = ExecutionManager.executeCommand(message, mClient.getInetAddress().toString(), mFeedback);
			dos.writeInt(response.length);
			dos.write(response, 0, response.length);		    
			dos.flush();
			bufferedReader.close();
			dos.close();
			mClient.close();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
