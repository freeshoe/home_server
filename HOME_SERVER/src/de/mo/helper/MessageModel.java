package de.mo.helper;

import de.mo.options.OPTIONS;

public class MessageModel {
	
	private int mGroupChoice;
	private int mCommandChoice;	
	private String mExtraMessage;
	private boolean mValidResponse;
	
	/**
	 * Erstellt ein neues MessageModel mit gegebenen Parametern.
	 * @param groupChoice Gruppen-Kommando
	 * @param commandChoice Spezifiches Kommando
	 * @param extraMessage Zusatznachricht, null wird zu "null"
	 */
	public MessageModel(int groupChoice, int commandChoice, String extraMessage){
		this.mGroupChoice = groupChoice;
		this.mCommandChoice = commandChoice;
		if(extraMessage==null)
			this.mExtraMessage = "null";
		else
			this.mExtraMessage = Utilities.normalizeMessage(extraMessage);
		this.mValidResponse = true;
	}
	
	/**
	 * Erstellt ein neues MessageModel aus einem String mit Trennzeichen (OPTIONS.COMMANDSPLITTER).
	 * @param groupChoice Gruppen-Kommando
	 * @param commandChoice Spezifiches Kommando
	 * @param extraMessage Zusatznachricht
	 */
	public MessageModel(String response){
		String[] commandSplitted = response.split("#");
		if(commandSplitted.length==OPTIONS.COMMANDLENGTH){
			this.mGroupChoice = Integer.parseInt(commandSplitted[0]);
			this.mCommandChoice = Integer.parseInt(commandSplitted[1]);
			this.mExtraMessage = Utilities.normalizeMessage(commandSplitted[2]);
			this.mValidResponse = true;
		}
		else
			this.mValidResponse = false;
			
	}

	/**
	 * OPTIONS (1 - 999)
	 * @return OPTIONS
	 */
	public int getGroupChoice() {
		return mGroupChoice;
	}

	/**
	 * OPTIONS_MUSIC, OPTIONS_VIDEO, OPTIONS_PLAYERBAR (1 - 999)
	 * @return
	 */
	public int getCommandChoice() {
		return mCommandChoice;
	}

	/**
	 * Extramessage
	 * @return
	 */
	public String getExtraMessage() {
		return mExtraMessage;
	}
	
	/**
	 * G�ltig wenn alle Werte eingetragen (Extramessage kann null sein)
	 * @return
	 */
	public boolean isValid(){
		return mValidResponse;
	}
	
	/**
	 * Gibt das Modell als String zur�ck
	 * @return MessageModel toString
	 */
	public String messageModelToString(){
		return String.valueOf(mGroupChoice)+OPTIONS.COMMANDSPLITTER+		// 1
				String.valueOf(mCommandChoice)+OPTIONS.COMMANDSPLITTER+		// 2
				mExtraMessage;												// 3
	}
}
