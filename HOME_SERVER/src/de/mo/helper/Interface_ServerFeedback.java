package de.mo.helper;

import de.mo.function.music.MusicPlayer;
import de.mo.function.video.VideoPlayer;

public interface Interface_ServerFeedback {
	public MusicPlayer getMusicPlayer();
	public VideoPlayer getVideoPlayer();
}
