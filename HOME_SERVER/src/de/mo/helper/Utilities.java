package de.mo.helper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {
	
	/*/**
	 * Entfernt alle OPTIONS.COMMANDSPLITTER und erstetzt diese mit "�". 
	 * @param text
	 * @return text
	 *//*
	public static String convertTextToRequest(String text){
		String tmp = text.replaceAll(OPTIONS.COMMANDSPLITTER, "�");
		return tmp;
	}
	
	/**
	 * Umkehrfunktion von convertTextToRequest
	 * @param request
	 * @return request
	 *//*
	public static String convertRequestToText(String request){
		String tmp = request.replaceAll("�", OPTIONS.COMMANDSPLITTER);
		return tmp;
	}*/
	
	/**
	 * Entfernt alle ung�ltigen Eingaben.
	 * @param message
	 * @return normalized message
	 */
	public static String normalizeMessage(String message){
		StringBuilder sb = new StringBuilder();
		for(char c : message.toCharArray()){
			if((c >= 'a' && c <= 'z') || ((c >= 'A') && (c <= 'Z')) ||
					(Character.isDigit(c)) || (c == '(') || (c == ')') ||
						(c == '-') || (c == ' ') || (c == '.') || (c == '/') || 
							(c == '?') || (c == '=') || (c == ':') || (c == '_'))
				sb.append(c);
		}
		return sb.toString();
	}
	
	/**
	 * Konvertiert Umlaute
	 * @param text
	 * @return text
	 */
	public static String convertUmlaut(String text){
		return text.replaceAll("�", "ae").replaceAll("�", "oe").replaceAll("�", "ue").replaceAll("�", "AE").replaceAll("�", "OE").replaceAll("�", "UE");
	}

	/**
	 * Zeitstempel ("dd-MM-yyyy'-'HH:mm:ss") als String
	 * @return Zeitstempel
	 */
	public static String getTimeStamp(){
		return new SimpleDateFormat("dd-MM-yyyy'-'HH:mm:ss").format(new Date());
	}
	
	/**
	 * F�hrt das String-Array in der Runtime aus und schlie�t diese ohne auslesen.
	 * @param command
	 */
	public static void executeArray(String[] command){
		Runtime mRuntime = Runtime.getRuntime();
		try {
			Process process = mRuntime.exec(command);
			process.getErrorStream().close();
			process.getInputStream().close();
			process.getOutputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
