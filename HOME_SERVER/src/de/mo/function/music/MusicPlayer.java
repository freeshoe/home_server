package de.mo.function.music;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import de.mo.helper.Utilities;
import de.mo.paths.DirectionPaths;

public class MusicPlayer implements StateInterface{
	
	private List<String> mPlayList;
	private List<String> mUrlList;
	private int mActualTrackIndex;
	private boolean mMusicRunning;
	private Player mPlayer;
	private MusikStateThread mThread;
	
	public MusicPlayer(){
		mActualTrackIndex = 0;
		mMusicRunning = false;
		refreshPlayList();
	}
	
	/**
	 * Erneuert die Playlist sodass neue Dateien angezeigt werden
	 */
	public void refreshPlayList(){
		mPlayList = null;
		mPlayList = new ArrayList<String>();
		mUrlList = null;
		mUrlList = new ArrayList<String>();
		//PI: File folder = new File("/home/pi/Desktop/Music");
		//MAC: File folder = new File("/Users/Mo/Desktop/mukke/Server_Tracks");
		//File folder = new File("/home/pi/Desktop/Music");
		File folder = new File(DirectionPaths.HAUPTPATH);
		File[] listOfFiles = folder.listFiles();
		String tmp = null;
		for(int i = 0; i < listOfFiles.length; i++){
			if(listOfFiles[i].isFile()){
				if(checkIfRightFile(listOfFiles[i])){
					tmp = listOfFiles[i].getName();
					tmp = Utilities.normalizeMessage(tmp);
			        mPlayList.add(tmp);
			        mUrlList.add(listOfFiles[i].getPath());
				}			
			}
		}
	}
	
	/**
	 * Gibt die Playlist als String-Array zur�ck (Dateinamen)
	 * @return String[] playList 
	 */
	public ArrayList<String> getPlayListArray(){
		ArrayList<String> tmp = new ArrayList<String>();
		for(String name : mPlayList){
			tmp.add(name);
		}
		return tmp;
	}
	
	
	
	/**
	 * Hauptfunktion zum Abspielen
	 * @param index Index des Lieds das abgespielt werden soll
	 */
	public void playTrackByIndex(int index){
		if(index>=mPlayList.size() || index<0)index=0; // Setzt aktuellen Index auf 0 wenn index > array.length  oder index < 0
		mActualTrackIndex = index;
		
		killPlayer(); // Killt den Player (falls aktuelle Wiedergabe)
		if(!mMusicRunning)startThread();
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(mUrlList.get(index)); // FIS mit entsprechender URL
			mPlayer = new Player(fis);
			Thread t = new Thread(new Runnable() { public void run() { 
				try {
					mPlayer.play();
				} catch (JavaLayerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				}});
			t.start();
			startThread();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JavaLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mMusicRunning = true;
	}
	
	/**
	 * Sucht den Track und spielt ihn ab
	 * @param trackName
	 * @return
	 */
	public byte[] playTrackByName(String trackName){
		int tmp = getTrackIndex(trackName);
		if(tmp==-1)return new byte[]{0};
		playTrackByIndex(tmp);
		return new byte[]{1};
	}

	/**
	 * Position
	 * @param trackName
	 * @return int pos
	 */
	public int getTrackIndex(String trackName){
		return mPlayList.indexOf(trackName);
	}
	
	/**
	 * Stoppt die Wiedergabe.
	 */
	public void stopPlayer(){
		if(mMusicRunning){
			mMusicRunning = false; // Vor killPlayer() auf false setzen damit der Thread geschlossen wird
			killPlayer();
		}	
	}
	
	/**
	 * Startet die Wiedergabe.
	 */
	public void playPlayer(){
		if(!mMusicRunning){
			if(mPlayer!=null){
				playTrackByIndex(mActualTrackIndex);
			}
		}	
	}
	
	/**
	 * N�chstes Lied in der Liste, wenn index > List.size wird das 1. wiedergegeben.
	 */
	public void nextPlayer(){
		if(mPlayer!=null){
			killPlayer();
			playTrackByIndex(mActualTrackIndex+1);
		}
	}
	
	/**
	 * Das vorherige wird wiedergegeben.
	 */
	public void previousPlayer(){
		if(mPlayer!=null){
			killPlayer();
			playTrackByIndex(mActualTrackIndex-1);
		}
	}
	/**
	 * Sucht den String in der Playlist (Volltextsuche)
	 * @param search String 
	 * @return ArrayList<String> die gefundenen Tracks
	 */
	public ArrayList<String> searchTracks(String search){
		ArrayList<String> tmp = new ArrayList<String>();
		for(String entry : mPlayList){
			if(entry.toLowerCase().contains(search.toLowerCase()))tmp.add(entry);
		}
		return tmp;
	}
	
	/**
	 * Gibt true zur�ck wenn die Musik l�uft
	 * @return
	 */
	public boolean getPlayerStatus(){
		return mMusicRunning;
	}
	
	
	
	
	
	/*
	 * PRIVATE-SECTION
	 */
	// HAUPTMETHODE ZUM SCHLIE�EN DES PLAYERS!!!
	private void killPlayer(){
		if(mPlayer!=null){
			mPlayer.close();
			if(!mMusicRunning)
				killThread();		
		}
	}
	
	// Startet den StateThread
	private void startThread(){
		if(mThread==null){
			mThread = new MusikStateThread(this);
			mThread.setRunning(true);
			mThread.start();
		}
	}
	
	// Stoppt den StateThread
	private void killThread(){
		if(mThread!=null){	
			mThread.setRunning(false);
			try {
				mThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mThread = null;
		}
	}
	
	// �berpr�ft ob das File die Endung ".mp3" hat
	private boolean checkIfRightFile(File file){
		String tmp = file.getName();
		if(tmp.length()>4)
			if(tmp.substring(tmp.length()-4, tmp.length()).equals(".mp3") && !tmp.substring(0,0).equals("."))return true;
		return false;
	}
	
	
	

	
	/*
	 *  Interfaces f�r den Thread
	 */
	@Override
	public boolean trackCompleted() {
		if(mPlayer!=null)
			return mPlayer.isComplete();
		else return false;
	}

	@Override
	public void playingFinished() {
		nextPlayer();
	}
}
