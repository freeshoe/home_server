package de.mo.function.music;

public interface StateInterface {
	/**
	 * Gibt true zur�ck wenn die Wiedergabe des Tracks abgeschlossen wurde
	 * @return true, wenn Track komplett wiedergegeben
	 */
	public boolean trackCompleted();
	
	/**
	 * Wird aufgerufen wenn Wiedergabe abgeschlossen
	 */
	public void playingFinished();
}
