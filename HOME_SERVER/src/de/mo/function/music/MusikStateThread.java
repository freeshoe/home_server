package de.mo.function.music;

public class MusikStateThread extends Thread{
	
	private boolean mRunning;
	private StateInterface mState;
	
	public MusikStateThread(StateInterface state){
		this.mState = state;
		mRunning = true;
	}
	
	@Override
	public void run(){
		while(mRunning){
			if(mState.trackCompleted())mState.playingFinished();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setRunning(boolean running){
		this.mRunning = running;
	}
	
	public boolean getRunning(){
		return mRunning;
	}
	
}
