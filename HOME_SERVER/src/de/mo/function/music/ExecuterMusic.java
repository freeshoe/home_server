package de.mo.function.music;

import java.nio.charset.Charset;
import java.util.ArrayList;

import de.mo.execution.ExecutionManager;
import de.mo.helper.Interface_ServerFeedback;
import de.mo.options.OPTIONS_MUSIC;
import de.mo.options.OPTIONS_PLAYERBAR;

public class ExecuterMusic {
	
	public static byte[] executeCommand(int command, String extramessage, Interface_ServerFeedback feedback){
		byte[] tmp = ExecutionManager.FAILED;
		switch(command){
			case OPTIONS_MUSIC.MUSICLIST:
				StringBuilder sb = new StringBuilder();
				feedback.getMusicPlayer().refreshPlayList();
				ArrayList<String> musiclist = feedback.getMusicPlayer().getPlayListArray();
				if(musiclist==null)break;
				for(int i=0; i<musiclist.size(); i++){
					if(musiclist.get(i)!=null)
						sb.append(musiclist.get(i).replaceAll("`", "'")+"`");
				}
				tmp = sb.toString().getBytes(Charset.forName("UTF-8"));
				break;
			case OPTIONS_MUSIC.PLAYTRACK:
				if(feedback.getVideoPlayer().getPlayerState())feedback.getVideoPlayer().stopPlayer();
				tmp = feedback.getMusicPlayer().playTrackByName(extramessage);
				break;
			case OPTIONS_PLAYERBAR.PLAY:
				feedback.getMusicPlayer().playPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.STOP:
				feedback.getMusicPlayer().stopPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.NEXT:
				feedback.getMusicPlayer().nextPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.PREVIOUS:
				feedback.getMusicPlayer().previousPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.SEARCH:
				StringBuilder sb2 = new StringBuilder();
				feedback.getMusicPlayer().refreshPlayList();
				ArrayList<String> musiclist2 = feedback.getMusicPlayer().searchTracks(extramessage);
				if(musiclist2==null)break;
				for(int i=0; i<musiclist2.size(); i++){
					if(musiclist2.get(i)!=null)
						sb2.append(musiclist2.get(i).replaceAll("`", "'")+"`");
				}
				tmp = sb2.toString().getBytes(Charset.forName("UTF-8"));
				break;
		}
		return tmp;
	}

}
