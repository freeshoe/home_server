package de.mo.function.video;

import java.nio.charset.Charset;
import java.util.ArrayList;

import de.mo.execution.ExecutionManager;
import de.mo.helper.Interface_ServerFeedback;
import de.mo.options.OPTIONS;
import de.mo.options.OPTIONS_PLAYERBAR;
import de.mo.options.OPTIONS_VIDEO;

public class ExecuterVideo {
	
	public static byte[] executeCommand(int command, String extramessage, Interface_ServerFeedback feedback){
		byte[] tmp = ExecutionManager.FAILED;
		switch(command){
			case OPTIONS_VIDEO.VIDEOLIST:
				StringBuilder sb = new StringBuilder();
				feedback.getVideoPlayer().refreshPlayList();
				ArrayList<String> videolist = feedback.getVideoPlayer().getPlayListArray();
				if(videolist==null)break;
				for(int i=0; i<videolist.size(); i++){
					if(videolist.get(i)!=null)
						sb.append(videolist.get(i).replaceAll(OPTIONS.LISTSPLITTER, "'")+OPTIONS.LISTSPLITTER);
				}
				tmp = sb.toString().getBytes(Charset.forName("UTF-8"));
				break;
			case OPTIONS_VIDEO.PLAYVIDEO:
				if(feedback.getMusicPlayer().getPlayerStatus())feedback.getMusicPlayer().stopPlayer();
				feedback.getVideoPlayer().playVideoByName(extramessage);
				//Utilities.executeArray(new String[]{"omxplayer", feedback.getVideoPlayer().getVideoUrl(extramessage)});
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_VIDEO.PLAYVIDEOSTREAM:
				if(feedback.getMusicPlayer().getPlayerStatus())feedback.getMusicPlayer().stopPlayer();
				feedback.getVideoPlayer().playVideoStream(extramessage);
				break;
			case OPTIONS_PLAYERBAR.PLAY:
				feedback.getVideoPlayer().startPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.STOP:
				feedback.getVideoPlayer().stopPlayer();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_PLAYERBAR.SEARCH:
				StringBuilder sb2 = new StringBuilder();
				feedback.getVideoPlayer().refreshPlayList();
				ArrayList<String> videolist2 = feedback.getVideoPlayer().searchTracks(extramessage);
				if(videolist2==null)break;
				for(int i=0; i<videolist2.size(); i++){
					if(videolist2.get(i)!=null)
						sb2.append(videolist2.get(i).replaceAll(OPTIONS.LISTSPLITTER, "'")+OPTIONS.LISTSPLITTER);
				}
				tmp = sb2.toString().getBytes(Charset.forName("UTF-8"));
				break;
			case OPTIONS_VIDEO.DECREASE:
				feedback.getVideoPlayer().decreaseSpeed();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_VIDEO.INCREASE:
				feedback.getVideoPlayer().increaseSpeed();
				tmp = ExecutionManager.DONE;
				break;
			case OPTIONS_VIDEO.PAUSE:
				feedback.getVideoPlayer().pausePlayer();
				tmp = ExecutionManager.DONE;
				break;
		}
		return tmp;
	}

}
