package de.mo.function.video;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import de.mo.helper.Utilities;
import de.mo.options.OPTIONS_OMXPLAYER;
import de.mo.paths.DirectionPaths;

public class VideoPlayer {
	
	private List<String> mPlayList;
	private List<String> mUrlList;
	private boolean mRunning;
	private Runtime mRuntime;
	private BufferedWriter mPipeline;
	private int mLastVideoIndex;
	private boolean mPause;
	
	public VideoPlayer(){
		mRunning = false;
		mPause = false;
		mRuntime = null;
		mPipeline = null;
		mLastVideoIndex = -1;
		refreshPlayList();
	}
	
	/**
	 * Erneuert die Wiedergabeliste (falls neue Dateien)
	 */
	public void refreshPlayList(){
		mPlayList = null;
		mPlayList = new ArrayList<String>();
		mUrlList = null;
		mUrlList = new ArrayList<String>();		
		File folder = new File(DirectionPaths.HAUPTPATH);
		File[] listOfFiles = folder.listFiles();
		String tmp = null;
		for(int i = 0; i < listOfFiles.length; i++){
			if(listOfFiles[i].isFile()){
				if(checkIfRightFile(listOfFiles[i])){
					tmp = listOfFiles[i].getName();
					tmp = Utilities.normalizeMessage(tmp);
			        mPlayList.add(tmp);
			        mUrlList.add(listOfFiles[i].getPath());
				}			
			}
		}
	}
	
	/**
	 * Gibt die Video-Playlist zur�ck
	 * @return Video-Playlist
	 */
	public ArrayList<String> getPlayListArray(){
		ArrayList<String> tmp = new ArrayList<String>();
		for(String name : mPlayList){
			tmp.add(name);
		}
		return tmp;
	}
	
	/**
	 * HAUPTFUNTKION
	 * @param index zum Abspielen
	 */
	public void playVideoByIndex(int index){
		if(mRunning)killRuntime();
		String url = mUrlList.get(index);
		mRuntime = Runtime.getRuntime();
		try {
			Process process = mRuntime.exec(OPTIONS_OMXPLAYER.openPlayer(url));
			mPipeline = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			process.getErrorStream().close();
			process.getInputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mLastVideoIndex = index;
		mRunning = true;		
	}
	
	
	/**
	 * Spielt das Video mit dem �bergebenen Namen ab
	 * @param name Video das abgespielt werden soll
	 */
	public void playVideoByName(String name){
		playVideoByIndex(mPlayList.indexOf(name));
	}
	
	/**
	 * Stoppt den Player
	 */
	public void stopPlayer(){
		if(mRunning)killRuntime();
	}
	
	/**
	 * �berpr�ft, ob pausiert wurde. Wenn ja, setzt er die Wiedergabe fort.
	 * Ansonsten wird der Player mit dem letzten aufgerufenen Video gestartet, falls vorhanden.
	 * Falls nicht vorhanden, startet er das 1. Video in der Liste.
	 * Falls auch nicht vorhanden, tut er nichts.
	 */
	public void startPlayer(){
		if(mPause){
			pausePlayer();
			mPause = true;
		}
		else if(mLastVideoIndex>-1)playVideoByIndex(mLastVideoIndex);
		else if(mPlayList.size()>0)playVideoByIndex(0);
	}
	
	/**
	 * Gibt die Video-Url zur�ck
	 * @param name
	 * @return String (Url)
	 */
	public String getVideoUrl(String name){
		int index = mPlayList.indexOf(name);
		return mUrlList.get(index);
	}
	
	/**
	 * Gibt den aktuellen Wiedergabestatus zur�ck
	 * @return Wiedergabestatus, true wenn am Abspielen
	 */
	public boolean getPlayerState(){
		return mRunning;
	}
	
	/**
	 * Sucht den String in der Playlist (Volltextsuche)
	 * @param search String 
	 * @return ArrayList<String> die gefundenen Videos
	 */
	public ArrayList<String> searchTracks(String search){
		ArrayList<String> tmp = new ArrayList<String>();
		for(String entry : mPlayList){
			if(entry.toLowerCase().contains(search.toLowerCase()))tmp.add(entry);
		}
		return tmp;
	}
	
	/**
	 * Spielt den �bergebenen YouTube-Link ab (extrahiert vorher den RTSP-Link).
	 * @param http YouTube-Link
	 */
	public void playVideoStream(String http){
		if(mRunning)killRuntime();
		try {
			String tmp = runtimeExec(OPTIONS_OMXPLAYER.getPlayerStream(http));
			mRuntime = Runtime.getRuntime();
			Process processStart = mRuntime.exec(OPTIONS_OMXPLAYER.openPlayer(tmp));
			mPipeline = new BufferedWriter(new OutputStreamWriter(processStart.getOutputStream()));
			processStart.getErrorStream().close();
			processStart.getInputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mRunning = true;	
	}
	
	/**
	 * Lautst�rke +
	 */
	public void volumeUp(){
		if(mPipeline!=null){
			try {
				mPipeline.write("+");
				mPipeline.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * Lautst�rke -
	 */
	public void volumeDown(){
		if(mPipeline!=null){
			try {
				mPipeline.write("-");
				mPipeline.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * Erh�ht die Wiedergabegeschwindigkeit
	 */
	public void decreaseSpeed(){
		if(mPipeline!=null){
			try {
				mPipeline.write("^[[D");
				//mPipeline.write(KeyEvent.VK_LEFT);
				mPipeline.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //mRobot.keyPress(KeyEvent.VK_LEFT);
			 //mRobot.keyRelease(KeyEvent.VK_LEFT);
		}	
	}
	
	/**
	 * Verringert die Wiedergabegeschwindigkeit
	 */
	public void increaseSpeed(){
		if(mPipeline!=null){
			try {
				mPipeline.write("^[[C");
				//mPipeline.write(KeyEvent.VK_RIGHT);
				mPipeline.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//mRobot.keyPress(KeyEvent.VK_RIGHT);
			//mRobot.keyRelease(KeyEvent.VK_RIGHT);
		}	
	}
	
	/**
	 * Pausiert den Player (kann fortgesetzt werden)
	 */
	public void pausePlayer(){
		if(mPipeline!=null){
			try {
				mPipeline.write("p");
				mPipeline.flush();
				mPause = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	private String runtimeExec(String[] commands){
		mRuntime = Runtime.getRuntime();
		String tmp = "";
		try {
			Process process = mRuntime.exec(commands);
			BufferedReader err=
		             new BufferedReader(new InputStreamReader(process.getInputStream())); 
			tmp = err.readLine();
			process.getErrorStream().close();
			process.getInputStream().close();
			process.getOutputStream().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return tmp;
	}
	
	//Runtime Exit
	private void killRuntime(){
		if(mRuntime!=null){
			try {
				mPipeline.close();
				mRuntime.exec(OPTIONS_OMXPLAYER.KILLPLAYER);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mPipeline = null;
			mRuntime = null;
			mRunning = false;
		}		
	}
	
	// �berpr�ft ob das File die Endung ".mp4" hat
	private boolean checkIfRightFile(File file){
		String tmp = file.getName();
		if(tmp.length()>4)
			if(tmp.substring(tmp.length()-4, tmp.length()).equals(".mp4") && !tmp.substring(0,0).equals("."))return true;
		return false;
	}

}
