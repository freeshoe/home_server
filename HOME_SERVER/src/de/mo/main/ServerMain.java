package de.mo.main;

import java.util.Scanner;

public class ServerMain {
	
	private static ServerMainThread mServerMainThread;

	/**
	 * START (KONSOLEN-VERSION)
	 * @param args
	 */
	public static void main(String[] args) {
		scanInput();
	}

	/**
	 * START
	 */
	private static void startServer(){

		System.out.println("open");
		if(mServerMainThread==null){
			mServerMainThread = new ServerMainThread();
			mServerMainThread.start();
		}
		scanInput();
	}
	
	/**
	 * STOP
	 */
	private static void closeServer(){
		System.out.println("close");
		if(mServerMainThread!=null){	
			try {
				mServerMainThread.closeServer();
				mServerMainThread.join();				
				mServerMainThread = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		scanInput();
	}
	
	/**
	 * Scannt den input
	 */
	public static void scanInput(){
		//System.out.print("Input: ");
		Scanner sc = new Scanner(System.in);
		String tmp = sc.next();
		if(tmp.equals("y"))startServer();
		else closeServer();
	}
	
}
