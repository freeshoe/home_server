package de.mo.main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import de.mo.function.music.MusicPlayer;
import de.mo.function.video.VideoPlayer;
import de.mo.helper.ClientHandler;
import de.mo.helper.Interface_ServerFeedback;
import de.mo.options.OPTIONS;

public class ServerMainThread extends Thread implements Interface_ServerFeedback{

	private ServerSocket mServerSocket;
	
	/**
	 * FUNCTIONS
	 */
	private MusicPlayer mMusicPlayer;
	private VideoPlayer mVideoPlayer;
	
	/**
	 * Hauptthread der auf dem angewiesenen Port lauscht und die Clients an Threads weitergibt.
	 */
	public ServerMainThread(){
		mMusicPlayer = new MusicPlayer();
		mVideoPlayer = new VideoPlayer();
	}
		
	public void run(){
		Socket client = null;
		try {
			mServerSocket = new ServerSocket(OPTIONS.PORT);
			System.out.println("Server started...");
			while(!mServerSocket.isClosed()){
				client = mServerSocket.accept();
				handleClient(client);
			}		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Schlie�t Socket und stoppt alle Wiedergaben
	 */
	public void closeServer(){
		try {
			mServerSocket.close();
			mMusicPlayer.stopPlayer();
			mVideoPlayer.stopPlayer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void handleClient(Socket client) throws IOException{
		ClientHandler t = new ClientHandler(client, this);
		t.start();	
	}

	@Override
	public MusicPlayer getMusicPlayer() {
		// TODO Auto-generated method stub
		return mMusicPlayer;
	}

	@Override
	public VideoPlayer getVideoPlayer() {
		// TODO Auto-generated method stub
		return mVideoPlayer;
	}

}
