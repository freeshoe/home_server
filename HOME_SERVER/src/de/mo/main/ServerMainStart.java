package de.mo.main;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;


public class ServerMainStart extends JFrame implements ActionListener{

	/**
	 * UNBENUTZT
	 * Main mit Gui
	 */
	private static final long serialVersionUID = 1L;
	private ServerMainThread mServerMainThread;
	
	public ServerMainStart(){
		super("Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new FlowLayout());
		JButton start = new JButton("start");
		JButton stop = new JButton("stop");
		
		start.addActionListener(this);
		stop.addActionListener(this);
		
		getContentPane().add(start);
		getContentPane().add(stop);
		
		mServerMainThread = null;
		setVisible(true);
		
		//getScanner();
	}

	/*public static void main(String[] args) {
		new ServerMainStart();
	}*/
	
	/**
	 * START
	 */
	private void startServer(){
		if(mServerMainThread==null){
			mServerMainThread = new ServerMainThread();
			mServerMainThread.start();
		}
		//getScanner();
	}
	
	/**
	 * STOP
	 */
	private void closeServer(){
		if(mServerMainThread!=null){	
			try {
				mServerMainThread.closeServer();
				mServerMainThread.join();				
				mServerMainThread = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//getScanner();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println(arg0.getActionCommand());
		String command = arg0.getActionCommand();
		if(command.equals("start")){
			startServer();
		}
		else if(command.equals("stop")){
			closeServer();
		}
	}
}
