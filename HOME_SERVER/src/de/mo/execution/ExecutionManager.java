package de.mo.execution;

import de.mo.function.music.ExecuterMusic;
import de.mo.function.video.ExecuterVideo;
import de.mo.helper.Interface_ServerFeedback;
import de.mo.helper.MessageModel;
import de.mo.helper.Utilities;
import de.mo.options.OPTIONS;

public class ExecutionManager {
	
	public final static int COMMANDLENGTH = 4;
	public final static byte[] FAILED = {0};
	public final static byte[] DONE = {1};
	
	public static byte[] executeCommand(MessageModel message, String ip, Interface_ServerFeedback feedback){
		if(!message.isValid()){
			System.out.println("Message failed from "+ip);
			return FAILED;
		}
		System.out.println(Utilities.getTimeStamp() + " - IP: " + ip + " - CommandGroup: " + message.getGroupChoice() + " - Command: " + message.getCommandChoice() + " - ExtraMessage: " + message.getExtraMessage());
		switch(message.getGroupChoice()){
			case OPTIONS.MUSIC:
				return ExecuterMusic.executeCommand(message.getCommandChoice(), message.getExtraMessage(), feedback);
			case OPTIONS.VIDEO:
				return ExecuterVideo.executeCommand(message.getCommandChoice(), message.getExtraMessage(), feedback);
			case OPTIONS.SYSTEM:
				return SystemExecuter.executeCommand(message.getCommandChoice(), message.getExtraMessage(), feedback);
			default:
				return FAILED;
		}
	}
}



