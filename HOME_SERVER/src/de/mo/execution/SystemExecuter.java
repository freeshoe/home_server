package de.mo.execution;

import de.mo.helper.Interface_ServerFeedback;
import de.mo.helper.Utilities;
import de.mo.options.OPTIONS;
import de.mo.options.OPTIONS_AMIXER;
import de.mo.options.OPTIONS_SYSTEM;

public class SystemExecuter {

	public static byte[] executeCommand(int command, String extramessage, Interface_ServerFeedback feedback){
		byte[] tmp = OPTIONS.FAILED;
		switch(command){
			case OPTIONS_SYSTEM.VOLUME_MUSIC:
				Utilities.executeArray(OPTIONS_AMIXER.setVolume(Integer.parseInt(extramessage)));
				tmp = OPTIONS.DONE;
				break;
			case OPTIONS_SYSTEM.VOLUME_VIDEO:
				if(extramessage.equals("up"))feedback.getVideoPlayer().volumeUp();
				else feedback.getVideoPlayer().volumeDown();
				tmp = OPTIONS.DONE;
				break;
			case OPTIONS_SYSTEM.SYSTEM_GENERAL_STOP:
				feedback.getMusicPlayer().stopPlayer();
				feedback.getVideoPlayer().stopPlayer();
				tmp = OPTIONS.DONE;
				break;
		}
		return tmp;
	}
	
}
