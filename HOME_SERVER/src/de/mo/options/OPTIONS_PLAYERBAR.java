package de.mo.options;

public class OPTIONS_PLAYERBAR {
	
	public final static int PLAY = 999;
	public final static int STOP = 998;
	public final static int NEXT = 997;
	public final static int PREVIOUS = 996;
	//Nur in OPTIONS.MUSIC und OPTIONS.YOUTUBE verf�gbar
	public final static int SEARCH = 995;
	//Nur in OPTIONS.VIDEO und OPTIONS.YOUTUBE verf�gbar
	public final static int INCREASE = 994;
	public final static int DECREASE = 993;
	public final static int PAUSE = 992;

}
