package de.mo.options;

public class OPTIONS_VIDEO {
	
	public final static int VIDEOLIST = 1;
	public final static int PLAYVIDEO = 2;
	public final static int PLAYVIDEOSTREAM = 3;
	public final static int DECREASE = 4;
	public final static int INCREASE = 5;
	public final static int PAUSE = 6;
	
}
