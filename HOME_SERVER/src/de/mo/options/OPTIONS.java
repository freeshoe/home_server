package de.mo.options;

public class OPTIONS {
	
	/**
	 * APPLET
	 */
	public final static int MAXROWS = 30;
	
	/**
	 * COMMAND-TRANSFER-SECTION
	 */
	public final static int PORT = 15000;
	public final static int COMMANDLENGTH = 3;
	public final static String COMMANDSPLITTER = "#";
	public final static String LISTSPLITTER = "`";
	public final static byte[] DONE = {1};
	public final static byte[] FAILED = {0};
	
	/**
	 * COMMAND-GROUP-SECTION
	 */
	public static final int MUSIC = 1;
	public static final int VIDEO = 2;
	public static final int YOUTUBE = 3;
	
	public static final int SYSTEM = 999;

}
