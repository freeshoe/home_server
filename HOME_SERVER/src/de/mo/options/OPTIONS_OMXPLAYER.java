package de.mo.options;

public class OPTIONS_OMXPLAYER {
	
	private final static String OUTPUT_LOCAL = "local";
	private final static String OUTPUT_HDMI = "hdmi";

	/**
	 * Gibt ein String-Array zum �ffnen einer Runtime und Ausf�hren des Prozesses wieder.
	 * Der erste String behinhaltet den Playernamen ("omxplayer") und bekommt als Argument 
	 * den Path der .mp4 Datei. Damit �ffnet sich ein neuer Videoplayer und spielt den �ber-
	 * gebenen Path ab.
	 * @param url Path der .mp4
	 * @return String-Array zum Ausf�hren in Runtime
	 */
	public static String[] openPlayer(String url){
		return new String[]{"omxplayer", "-o", OUTPUT_HDMI, url};
	}
	
	/**
	 * Verweist auf ein Consolen-Programm, dass aus dem �bergebenen String, der einen 
	 * YouTube-Link repr�sentieret, einen RTSP-Link generieren wird.
	 * @param http YouTube-Link
	 * @return String-Array zum Ausf�hren in Runtime
	 */
	public static String[] getPlayerStream(String http){
		return new String[]{"youtube-dl", "-g", http};
	}
	
	/**
	 * Streamt den �bergebenen RTSP-Link mit dem Player ("omxplayer").
	 * @param playerStream RTSP-Link
	 * @return String-Array zum Ausf�hren in der Runtime
	 */
	public static String[] openPlayerStream(String playerStream){
		return new String[]{"omxplayer", "-o", OUTPUT_HDMI,  "\""+playerStream+"\""};
	}
	
	/**
	 * String-Array zum Ausf�hren in der Rumtime.
	 * Zerst�rt den aktuellen Player.
	 */
	public final static String[] KILLPLAYER = {"sudo", "killall", "omxplayer.bin"};
	
}
