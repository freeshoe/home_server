package de.mo.options;

public class OPTIONS_AMIXER {
	
	/**
	 * Beschreibt den minimalen Prozentsatz des Mixers.
	 * Ab 65% ist der Sound kaum noch h�rbar. Ab 90% ungenie�bar.
	 */
	public final static int MIN_FIX_VALUE = 65;
	public final static int MAX_FIX_VALUE = 90;
	
	/**
	 * Setzt die Ausgabelautst�rke der Audiobuchse. 
	 * @param volumePercent zwischen MIN_FIX_VALUE und MAX_FIX_VALUE
	 * @return String-Array zum Ausf�hren in der Runtime
	 */
	public static String[] setVolume(int volumePercent){
		if(volumePercent<=MIN_FIX_VALUE)volumePercent = 0; // Sodass ab unter 65 gleich 0 geschaltet wird!
		if(volumePercent>MAX_FIX_VALUE)volumePercent = MAX_FIX_VALUE;
		return new String[]{"amixer", "sset", "\'PCM\'", String.valueOf(volumePercent)+"%"};
	}

}
