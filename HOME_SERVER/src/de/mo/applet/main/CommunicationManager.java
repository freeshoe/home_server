package de.mo.applet.main;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import de.mo.helper.MessageModel;
import de.mo.options.OPTIONS;

public class CommunicationManager {
	
	/**
	 * Sendet den Request an die IP. Der Port kann �ber OPTIONS geregelt werden.
	 * @param MessageModel request, siehe MessageModel
	 * @param ip Ip
	 * @return Server-Reply String
	 */
	public static String sendRequest(MessageModel request, String ip){
		if(!ip.isEmpty() && ip != null){
			try {
				Socket socket = new Socket(ip, OPTIONS.PORT);
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
				pw.print(request.messageModelToString());
				pw.flush();
				InputStream in = socket.getInputStream();
			    DataInputStream dis = new DataInputStream(in);
			    int len = dis.readInt();
			    byte[] data = new byte[len];
			    if (len > 0) {
			        dis.readFully(data);
			    }
			    in.close();
			    dis.close();
				pw.close();
				socket.close();
				return new String(data);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
