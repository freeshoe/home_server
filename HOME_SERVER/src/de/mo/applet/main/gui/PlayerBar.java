package de.mo.applet.main.gui;

import java.awt.Button;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import de.mo.applet.main.CommunicationManager;
import de.mo.applet.main.InterfaceRefresh;
import de.mo.helper.MessageModel;
import de.mo.options.OPTIONS;
import de.mo.options.OPTIONS_AMIXER;
import de.mo.options.OPTIONS_PLAYERBAR;
import de.mo.options.OPTIONS_SYSTEM;
import de.mo.options.OPTIONS_VIDEO;

public class PlayerBar extends Panel implements ActionListener, ChangeListener{

	private static final long serialVersionUID = 8564787754273435729L;

	private Button mPrevious;
	private Button mNext;
	private Button mPlay;
	private Button mStop;
	private TextField mSearch;
	private Button mGoSearch;
	//OPTIONS.MUSIC
	private JSlider mSliderVolume;
	//OPTIONS.VIDEO
	private Button mDecrease;
	private Button mIncrease;
	private Button mPause;
	private Button mUp;
	private Button mDown;
	private String mIp;
	private int mChoice;
	private InterfaceRefresh mRefresh;
	private int mVolume;
	
	/**
	 * Konstruktor
	 * @param main MainStart welches actionListener implementiert
	 */
	public PlayerBar(String ip, int choice, InterfaceRefresh refresh){
		mChoice = choice;
		mIp = ip;
		this.mRefresh = refresh;
		mVolume = 0;
		
		//Suchfeld
		if(mChoice==OPTIONS.MUSIC || mChoice==OPTIONS.YOUTUBE){
			mSearch = new TextField(40);
			mSearch.setEditable(true);
					
			mGoSearch = new Button("search");		
			mGoSearch.addActionListener(this);		
			//add Buttons
			add(mSearch);
			add(mGoSearch);
		}
				
		//Musik
		if(mChoice==OPTIONS.MUSIC){
			mPrevious = new Button("previous");
			mNext = new Button("next");
			mPlay = new Button("play");
			mStop = new Button("stop");
			mPrevious.addActionListener(this);
			mNext.addActionListener(this);
			mPlay.addActionListener(this);
			mStop.addActionListener(this);
			add(mPrevious);
			add(mNext);
			add(mPlay);
			add(mStop);
			mSliderVolume = new JSlider(JSlider.HORIZONTAL, 0, OPTIONS_AMIXER.MAX_FIX_VALUE-OPTIONS_AMIXER.MIN_FIX_VALUE, OPTIONS_AMIXER.MAX_FIX_VALUE-OPTIONS_AMIXER.MIN_FIX_VALUE); // 0, 35, 35
			mSliderVolume.addChangeListener(this);
			add(mSliderVolume);
		}
		
		//Video + Youtube
		else if(mChoice==OPTIONS.VIDEO || mChoice==OPTIONS.YOUTUBE){
			mDecrease = new Button("<");
			mIncrease = new Button(">");
			mPause = new Button("||");
			mUp = new Button("+");
			mDown = new Button("-");
			mDecrease.addActionListener(this);
			mIncrease.addActionListener(this);
			mPause.addActionListener(this);
			mUp.addActionListener(this);
			mDown.addActionListener(this);
			add(mDecrease);
			add(mIncrease);
			add(mPause);
			add(mUp);
			add(mDown);
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String command = arg0.getActionCommand();
		if(command.equals("play")){
			MessageModel request = new MessageModel(mChoice, OPTIONS_PLAYERBAR.PLAY, null);
			CommunicationManager.sendRequest(request, mIp);
		}
		else if(command.equals("stop")){
			MessageModel request = new MessageModel(mChoice, OPTIONS_PLAYERBAR.STOP, null);
			CommunicationManager.sendRequest(request, mIp);
		}
		else if(command.equals("next")){
			MessageModel request = new MessageModel(mChoice, OPTIONS_PLAYERBAR.NEXT, null);
			CommunicationManager.sendRequest(request, mIp);
		}
		else if(command.equals("previous")){
			MessageModel request = new MessageModel(mChoice, OPTIONS_PLAYERBAR.PREVIOUS, null);
			CommunicationManager.sendRequest(request, mIp);
		}
		else if(command.equals("search")){
			String tmp = mSearch.getText();
			if(!tmp.isEmpty()){
				if(mChoice==OPTIONS.YOUTUBE){
					MessageModel request = new MessageModel(OPTIONS.VIDEO, OPTIONS_VIDEO.PLAYVIDEOSTREAM, tmp);
					CommunicationManager.sendRequest(request, mIp);
				}
				else{
					MessageModel request = new MessageModel(mChoice, OPTIONS_PLAYERBAR.SEARCH, tmp);
					mRefresh.searchFinished(CommunicationManager.sendRequest(request, mIp));
				}			
			}			
		}
		//OPTIONS.VIDEO
		else if(command.equals("<"))sendVideoCommand(OPTIONS_VIDEO.DECREASE);
		else if(command.equals(">"))sendVideoCommand(OPTIONS_VIDEO.INCREASE);
		else if(command.equals("||"))sendVideoCommand(OPTIONS_VIDEO.PAUSE);
		else if(command.equals("+"))volumeChanged("up");
		else if(command.equals("-"))volumeChanged("down");
	}

	/**
	 * NUR IN OPTIONS.MUSIC VERF�GBAR
	 */
	@Override
	public void stateChanged(ChangeEvent arg0) {
		JSlider source = (JSlider)arg0.getSource();
	    if (!source.getValueIsAdjusting()) {
	        mVolume = (int)source.getValue() + OPTIONS_AMIXER.MIN_FIX_VALUE;
	        volumeChanged(null);
	    }
		
	}
	
	private void volumeChanged(String manual){
		MessageModel model = null;
		if(mChoice==OPTIONS.MUSIC)
			model = new MessageModel(OPTIONS.SYSTEM, OPTIONS_SYSTEM.VOLUME_MUSIC, String.valueOf(mVolume));
		else 
			model = new MessageModel(OPTIONS.SYSTEM, OPTIONS_SYSTEM.VOLUME_VIDEO, manual);
		CommunicationManager.sendRequest(model, mIp);
	}
	
	private void sendVideoCommand(int command){
		MessageModel model = new MessageModel(OPTIONS.VIDEO, command, null);
		CommunicationManager.sendRequest(model, mIp);
	}
}
