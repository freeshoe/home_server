package de.mo.applet.main.gui;

import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import de.mo.applet.main.CommunicationManager;
import de.mo.helper.MessageModel;
import de.mo.options.OPTIONS;
import de.mo.options.OPTIONS_MUSIC;

public class MusicList extends Panel{
	
	private static final long serialVersionUID = 1L;
	private DefaultListModel mListModel;
	private String mIp;
	private boolean mIsSearchResult;
	
	public MusicList(String ip){
		this.mIsSearchResult = false;
		this.mIp = ip;
		mListModel = new DefaultListModel();
		mListModel.addElement("test");
		final JList mList = new JList(mListModel);		
		mList.setVisibleRowCount(OPTIONS.MAXROWS);
		MouseListener mouseListener = new MouseAdapter() {
		    public void mouseClicked(MouseEvent e) {
		        if (e.getClickCount() == 2) {
		        	MessageModel request = new MessageModel(OPTIONS.MUSIC, OPTIONS_MUSIC.PLAYTRACK, (String) mList.getSelectedValue());
		        	CommunicationManager.sendRequest(request, mIp);
		         }
		    }
		};
		mList.addMouseListener(mouseListener);
		JScrollPane pane = new JScrollPane(mList);
		add(pane);
		updateList();
	}
	
	/**
	 * Fordert die Liste neu an.
	 */
	public void updateList(){
		if(!mIsSearchResult){
			MessageModel request = new MessageModel(OPTIONS.MUSIC, OPTIONS_MUSIC.MUSICLIST, null);
			String[] playList = CommunicationManager.sendRequest(request, mIp).split("`");
			mListModel.clear();
			for(int i=0; i<playList.length; i++){
				mListModel.add(i, playList[i]);
			}
		}		
	}
	
	public void updateList(String[] entrys){
		mListModel.clear();
		for(int i=0; i<entrys.length; i++){
			mListModel.add(i, entrys[i]);
		}
	}
}
