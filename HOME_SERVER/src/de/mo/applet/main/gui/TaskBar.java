package de.mo.applet.main.gui;

import java.awt.Button;
import java.awt.Panel;
import de.mo.applet.main.MainStart;

public class TaskBar extends Panel{
	
	private static final long serialVersionUID = 3370952245702734144L;
	//private JSlider mSliderVolume;
	//private int mVolume;
	//private String mIp;
	//private TextField mTextField;
	
	
	public TaskBar(MainStart main){
		//mTextField = new TextField(40);
		//mTextField.setEditable(true);
		//mIp = ip;
		//mVolume = 100;
		Button buttonMusic = new Button("Music");
		Button buttonMovies = new Button("Movies");
		Button buttonYouTube = new Button("YouTube");
		Button buttonGo = new Button("Refresh");
		Button buttonGeneralStop = new Button("STOP");
		//mSliderVolume = new JSlider(JSlider.HORIZONTAL, 0, 100-OPTIONS_AMIXER.MIN_FIX_VALUE, 100-OPTIONS_AMIXER.MIN_FIX_VALUE); // 0, 35, 35
		//mSliderVolume.addChangeListener(this);
		buttonMusic.addActionListener(main);
		buttonMovies.addActionListener(main);
		buttonYouTube.addActionListener(main);
		buttonGo.addActionListener(main);
		buttonGeneralStop.addActionListener(main);
		add(buttonMusic);
		add(buttonMovies);
		add(buttonYouTube);
		//add(mTextField);
		add(buttonGo);
		add(buttonGeneralStop);
		//add(mSliderVolume);
	}

	/*
	@Override
	public void stateChanged(ChangeEvent arg0) {
		JSlider source = (JSlider)arg0.getSource();
	    if (!source.getValueIsAdjusting()) {
	        mVolume = (int)source.getValue() + OPTIONS_AMIXER.MIN_FIX_VALUE;
	        volumeChanged();
	    }
		
	}
	
	private void volumeChanged(){
		MessageModel model = new MessageModel(OPTIONS.SYSTEM, mVolume, null);
		CommunicationManager.sendRequest(model, mIp);
	}
	 */
	/*public String getText(){
		return mTextField.getText();
	}*/
}
