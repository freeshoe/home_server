package de.mo.applet.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;

import de.mo.applet.main.gui.MusicList;
import de.mo.applet.main.gui.PlayerBar;
import de.mo.applet.main.gui.TaskBar;
import de.mo.applet.main.gui.VideoList;
import de.mo.helper.MessageModel;
import de.mo.options.OPTIONS;
import de.mo.options.OPTIONS_SYSTEM;

public class MainStart extends JApplet implements ActionListener, InterfaceRefresh{
	
	private static final long serialVersionUID = -7732015445084635217L;
	
	private boolean mIpInit;
	
	private boolean mMusicInit;
	private boolean mVideoInit;
	private boolean mYouTubeInit;
	
	private String mIp;
	
	private TaskBar mTaskBar;
	private PlayerBar mPlayerBar;
	private MusicList mMusicPane;
	private VideoList mVideoPane;
	
	private int mActualChoice;
	
	public MainStart(){	
		//Initials	
		mMusicInit = false;
		mVideoInit = false;
		mYouTubeInit = false;
		mIpInit = false;
		mIp = null;
		mActualChoice = 1; // Startet mit Musik		
	}

	public void init(){
		setSize(1000, 800);
		
		mIp = getDocumentBase().getHost();
		//mIp = "192.168.2.126"; // for testing
		
		if(!mIp.isEmpty()){
			
			getContentPane().setLayout(new BorderLayout());	
			
			//Textfield and Button for testing
			mTaskBar = new TaskBar(this);
			getContentPane().add("North", mTaskBar);
			
			setVisible(true);
			
			initialiseSite();
			mIpInit = true;
		}	
	}

	/**
	 * FOR THE PLAYERBAR!
	 */
	@Override
	public void actionPerformed(ActionEvent arg0){
		String command = arg0.getActionCommand();
		if(command.equals("Music")){
			mActualChoice = OPTIONS.MUSIC;
			initialiseSite();
		}
		else if(command.equals("Movies")){
			mActualChoice = OPTIONS.VIDEO;
			initialiseSite();	
		}
		else if(command.equals("YouTube")){
			mActualChoice = OPTIONS.YOUTUBE;
			initialiseSite();	
		}
		else if(command.equals("Refresh")){
			//String tmp = mTaskBar.getText(); 
			if(!mIp.isEmpty()){
				initialiseSite();
				mIpInit = true;
			}
			if(mIpInit)
				updateSite(false, null);
		}
		else if(command.equals("STOP")){
			MessageModel model = new MessageModel(OPTIONS.SYSTEM, OPTIONS_SYSTEM.SYSTEM_GENERAL_STOP, null);
			CommunicationManager.sendRequest(model, mIp);
		}
	}
	
	/*
	 * PRIVATE-SECTION
	 */
	
	/*
	 * Initialisiert die Liste auf die aktuelle Auswahl
	 */
	private void initialiseSite(){
		switch(mActualChoice){
			case OPTIONS.MUSIC:
				initialiseMusicSite();
				break;
			case OPTIONS.VIDEO:
				initialiseVideoSite();
				break;
			case OPTIONS.YOUTUBE:
				initialiseYouTubeSite();
				break;
			default:
				initialiseMusicSite();
				break;
		}
	}

	/*
	 * Aktualisiert die Liste der aktuellen Auswahl
	 */
	private void updateSite(boolean isSearchResult, String searchResult){
		switch(mActualChoice){
			case OPTIONS.MUSIC:
				if(!isSearchResult)
					mMusicPane.updateList();
				else
					mMusicPane.updateList(searchResult.split("`"));
				break;
			case OPTIONS.VIDEO:
				if(!isSearchResult)
					mVideoPane.updateList();
				else
					mVideoPane.updateList(searchResult.split("`"));
				break;
			default:
				break;
		}
	}
	
	private void clearSite(){
		switch(mActualChoice){
			case OPTIONS.MUSIC:
				if(mVideoInit){
					getContentPane().remove(mVideoPane);
					getContentPane().remove(mPlayerBar);
					mVideoInit = false;
				}
				else if(mYouTubeInit){
					getContentPane().remove(mPlayerBar);
					mYouTubeInit = false;
				}
				break;
				
			case OPTIONS.VIDEO:
				if(mMusicInit){
					getContentPane().remove(mMusicPane);
					getContentPane().remove(mPlayerBar);
					mMusicInit = false;
				}
				else if(mYouTubeInit){
					getContentPane().remove(mPlayerBar);
					mYouTubeInit = false;
				}
				break;
				
			case OPTIONS.YOUTUBE:
				if(mVideoInit){
					getContentPane().remove(mVideoPane);
					getContentPane().remove(mPlayerBar);
					mVideoInit = false;
				}
				else if(mMusicInit){
					getContentPane().remove(mMusicPane);
					getContentPane().remove(mPlayerBar);
					mMusicInit = false;
				}
				break;
		}
	}
	
	/*
	 * Music-Section
	 */
	// Initialisiert die Musikliste
	private void initialiseMusicSite(){
		clearSite();
		if(!mIp.isEmpty() && !mMusicInit){
			mMusicPane = new MusicList(mIp);
			mMusicPane.setPreferredSize(new Dimension(800, 500));
			getContentPane().add("Center", mMusicPane);	
			
			mPlayerBar = new PlayerBar(mIp, OPTIONS.MUSIC, this);
			getContentPane().add("South", mPlayerBar);
			
			mMusicInit = true;
			validate();
		}		
	}
	
	/*
	 * Video-Section
	 */
	//Initialisiert die Videoseite
	private void initialiseVideoSite(){
		clearSite();
		if(!mIp.isEmpty() && !mVideoInit){				
			mVideoPane = new VideoList(mIp);
			mVideoPane.setPreferredSize(new Dimension(800, 500));
			getContentPane().add("Center", mVideoPane);	
			
			mPlayerBar = new PlayerBar(mIp, OPTIONS.VIDEO, this);
			getContentPane().add("South", mPlayerBar);
			
			mVideoInit = true;
			validate();
		}	
	}
	
	private void initialiseYouTubeSite(){
		clearSite();
		if(!mIp.isEmpty() && !mYouTubeInit){		
			
			mPlayerBar = new PlayerBar(mIp, OPTIONS.YOUTUBE, this);
			getContentPane().add("South", mPlayerBar);
			
			mYouTubeInit = true;
			validate();
		}	
	}
	
	@Override
	public void searchFinished(String searchResult) {
		updateSite(true, searchResult);
	}
}
